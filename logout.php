<?php

$config = include_once('config.php');

if(isset($_SESSION["authkey"])){
	
	unset($_SESSION["authkey"]);
	header("Location: index.php");
}